package com.adalid.appusuarios.apirestspring.api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adalid.appusuarios.modelo.GestionUsuariosBD;
import com.adalid.appusuarios.modelo.Usuario;

@RestController
@RequestMapping("/api/usuarios")

public class UsuariosRestController {

	GestionUsuariosBD gesUsu;

	public UsuariosRestController() throws ClassNotFoundException {
		gesUsu = new GestionUsuariosBD();
	}

	@PostMapping
	public boolean nuevoUsuario(@RequestBody Usuario usu) {
		gesUsu.create(usu.nombre, usu.edad, usu.genero);
		return true;

	}

	public ArrayList<Usuario> leerTodos() {

		return gesUsu.mostrarTodos();
	}

}
