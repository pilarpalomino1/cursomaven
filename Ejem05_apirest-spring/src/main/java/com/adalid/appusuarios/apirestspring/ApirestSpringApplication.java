package com.adalid.appusuarios.apirestspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApirestSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApirestSpringApplication.class, args);
		System.out.println("*****   API REST ARRIBA    ***********");
	}

}
